$(document).ready(function() {
  var allPanels = $('.panel').hide();

  $(document).on('click', '.accordion', function() {
    allPanels.slideUp();
    if(this)
    $(this).next().slideDown();
    return false;
  });

  var clicked = false;

  $(document).on('click', '#theme', function() {
      if(clicked){
        $('.header').css('background-color', '#171B1A');
        $('body').css('background-color', '#FFFFFF');
        $('.panel').css('background-color', '#FFFFFF');
        $('body').css('color', 'black');
        $('button').css('color', 'white');
        clicked = false;
      }else{
        $('.header').css('background-color', '#000000');
        $('body').css('background-color', '#142634');
        $('.panel').css('background-color', '#4d4d4d');
        $('body').css('color', 'white');
        clicked = true;
      };
      return false;
  });
});
