$(document).ready(function() {
  var allPanels = $('.panel').hide();

  $('.accordion').click(function() {
    allPanels.slideUp();
    $(this).next().slideDown();
    return false;
  });
});
