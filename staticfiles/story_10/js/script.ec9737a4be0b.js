$(document).ready(function(){
    $(document).keypress(function(e){
      if(e.which === 13){
        $('#subscribe').click();
      }
    });

    $(document).on('click', '#subscribe', function() {
      $.ajax({
      type: "POST",
      url: "{% url 'subscribe' %}",
      dataType: 'json',
      success: function(response){
        console.log(response);
      });
    });
})
