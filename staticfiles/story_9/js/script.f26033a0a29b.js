$(document).ready(function(){
  var clicked = false;
    $.ajax({
      type: "GET",
      url:"https://www.googleapis.com/books/v1/volumes?q=quilting",
      dataType: "json",
      success: function(result){
        data = result.items;
        for(i = 0; i < data['length']; i++){
          var info = data[i].volumeInfo
          var tmp = "<tr><td>" + info.title + "</td><td>";
          for(j = 0; j < info.authors.length; j++){
            tmp = tmp + info.authors[j] + "br";
          }
          tmp = tmp + "</td>" + "<td class='right'><a><i id='star' class='fa fa-star checked'></i></a></td></tr>";
          $('#table_book > tbody').append(tmp);
        }
      }
    });

    $('#star').click( function() {
        if(clicked){
          $('#star').removeClass("checked");
          clicked = false;
        }else{
          $('#star').addClass("checked");
          clicked = true;
        };
        return false;
    });
})
