$(document).ready(function(){
    var fav = 0;
    var book_name = $('#search_input').val();
    var url = "getulululululul/";

    load = function(book_name){
      $.ajax({
      type: "GET",
      url: url + book_name,
      dataType: 'json',
      success: function(response){
        $('#table_book > tbody').empty();
        data = response
        for(i = 0; i < data.book.length; i++){
            var info = data.book[i]
            var tmp = "<tr><td>" + info.title + "</td><td>" + info.authors + "</td>" +
            "<td class='right'><button class='btn btn-outline-light'><i id='star' class='fa fa-star'></i></button></td></tr>";
            $('#table_book > tbody').append(tmp);
          }
        }
      });
    }

    load('quilting');

    $(document).on('click', '#search', function(){
      var book_name_local = $('#search_input').val();
      load(book_name_local);
    });

    $(document).keypress(function(e){
      if(e.which === 13){
        $('#search').click();
      }
    });

    $(document).on('click', '#star', function() {
        if($(this).hasClass("checked")){
          $(this).removeClass("checked");
          fav = fav - 1;
        }else{
          $(this).addClass("checked");
          fav = fav + 1;
        };
        $('#fav').html(fav + " favorited <i class='fa fa-star checked'></i>");
        return false;
    });
})
