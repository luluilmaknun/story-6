$(document).ready(function(){
    var fav = 0;

      $.ajax({
      type: "GET",
      url:"https://www.googleapis.com/books/v1/volumes?q=quilting",
      dataType: "json",
      success: function(result){
        data = result.items;
        for(i = 0; i < data['length']; i++){
          var info = data[i].volumeInfo
          var tmp = "<tr><td>" + info.title + "</td><td>";
          for(j = 0; j < info.authors.length; j++){
            tmp = tmp + info.authors[j] + "br";
          }
          tmp = tmp + "</td>" + "<td class='right'><button id='fav' class='btn btn-outline-light'><i id='star' class='fa fa-star'></i></button></td></tr>";
          $('#table_book > tbody').append(tmp);
        }
      }
    });

    $(document).on('click', '#star', function() {
        if($(this).hasClass("checked")){
          $(this).removeClas("checked");
          fav = fav - 1;
        }else{
          $(this).addClass("checked");
          fav = fav + 1;
        };
        $('#fav').html(fav + " favorited <i class='fa fa-star checked'></i>");
        return false;
    });
})
