$(document).ready(function() {
  var allPanels = $('.panel').hide();
  var active;
  var clicked = false;

  // $('.overlay').delay(2000);
  // $('.overlay').slideUp(500);

  $(document).on('click', '.accordion', function() {
    allPanels.slideUp();
    if(this === active){
      $(this).next().slideUp();
      $(this).removeClass('active');
      active = null;
      return false;
    }
    $(active).removeClass('active');
    $(this).addClass('active');
    $(this).next().slideDown();
    active = this;
    return false;
  });

  $('#theme').on('click', function() {
      if(clicked){
        $('.header').css('background-color', '#171B1A');
        $('body').css('background-color', '#FFFFFF');
        $('.panel').css('background-color', '#FFFFFF');
        $('body').css('color', 'black');
        $('button').css('color', 'white');
        $('#theme > h4').text('Night mode');
        clicked = false;
      }else{
        $('.header').css('background-color', '#000000');
        $('body').css('background-color', '#142634');
        $('.panel').css('background-color', '#4d4d4d');
        $('.accordion').css('background', '#000000');
        $('body').css('color', 'white');
        $('#theme > h4').text('Bright mode');
        clicked = true;
      };
      return false;
  });
});
