$(document).ready(function(){
    var fav = 0;
    var book_name = $('#search_input').val();
    var url = "getulululululul/";

    load = function(book_name){
      $.ajax({
      type: "GET",
      url: url + book_name,
      dataType: 'json',
      success: function(response){
        $('#table_book > tbody').empty();
        data = response;
        fav = data.counter;
        favorited = data.favorited_books;
        book = data.book;

        $('#fav').html(data.counter + " favorited <i class='fa fa-star checked'></i>");
        for(i = 0; i < book.length; i++){
            var info = book[i]
            if(typeof favorited != 'undefined'){
              if(favorited.includes(info.title)){
                var tmp = "<tr><td id='title'>" + info.title + "</td><td>" + info.authors + "</td>" +
                "<td class='right'><button class='btn btn-outline-light'><i id='star' class='fa fa-star checked'></i></button></td></tr>";
              }else{
                var tmp = "<tr><td id='title'>" + info.title + "</td><td>" + info.authors + "</td>" +
                "<td class='right'><button class='btn btn-outline-light'><i id='star' class='fa fa-star'></i></button></td></tr>";
              }
            }else{
              var tmp = "<tr><td id='title'>" + info.title + "</td><td>" + info.authors + "</td>" +
              "<td class='right'><button class='btn btn-outline-light'><i id='star' class='fa fa-star'></i></button></td></tr>";
            }
            $('#table_book > tbody').append(tmp);
          }
        }
      });
    }

    load('quilting');

    $(document).on('click', '#search', function(){
      var book_name_local = $('#search_input').val();
      load(book_name_local);
    });

    $(document).keypress(function(e){
      if(e.which === 13){
        $('#search').click();
      }
    });

    $(document).on('click', '#star', function() {
        var flag;
        if($(this).hasClass("checked")){
          $(this).removeClass("checked");
          fav = fav - 1;
          flag = 1;
        }else{
          $(this).addClass("checked");
          fav = fav + 1;
          flag = 0;
        };

        $('#fav').html(fav + " favorited <i class='fa fa-star checked'></i>");
        title = $(this).closest('td').prev().prev().text();

        $.ajax({
          type: "POST",
          url: '/story-9/books/',
          data: {'title' : title, 'flag' : flag},
          success: function(response){
            fav = response.counter;
          }
        });

        return false;
    });
})
