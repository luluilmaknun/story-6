# from django.test import TestCase, Client
# from django.urls import resolve
# from django.http import HttpRequest
# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
# from .views import *
# import time
#
# class Story9_Unit_Test(TestCase):
#     def test_url_is_exist(self):
#         response = Client().get('/story-9/')
#         self.assertEqual(response.status_code, 200)
#
#     def test_using_landing_template(self):
#         response = Client().get('/story-9/')
#         self.assertTemplateUsed(response, 'books.html')
#
#     def test_using_home_func(self):
#         found = resolve('/story-9/')
#         self.assertEqual(found.func, books)
#
# class Story9_Json_Test(TestCase):
#     def test_url_json_exist(self):
#         response = Client().get('/story-9/getulululululul/quilting/')
#         self.assertEqual(response.status_code, 200)
#
#     def test_using_json_func(self):
#         found = resolve('/story-9/getulululululul/quilting/')
#         self.assertEqual(found.func, readJSON)
#
# class Story9_Functional_Test(TestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('--disable-gpu')
#         self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         super(Story9_Functional_Test,self).setUp()
#
#     def tearDown(self):
#         self.selenium.quit()
#         super(Story9_Functional_Test, self).tearDown()
#
#     def test_change_theme_header(self):
#         selenium = self.selenium
#         selenium.get('http://127.0.0.1:8000/story-9/')
#         time.sleep(5);
#         css = selenium.find_element_by_tag_name('header')
#         background1 = css.value_of_css_property('background')
#
#         button = selenium.find_element_by_id('theme')
#         button.click()
#
#         background2 = css.value_of_css_property('background')
#         self.assertNotEqual(background1, background2)
#
#     def test_change_theme_background(self):
#         selenium = self.selenium
#         selenium.get('http://127.0.0.1:8000/story-9/')
#         time.sleep(5);
#         button = selenium.find_element_by_id('theme')
#         button.click()
#
#         css = selenium.find_element_by_tag_name('body')
#         background = css.value_of_css_property('background-color')
#         self.assertEqual(background, 'rgba(20, 38, 52, 1)')
#
#     def test_button_change_color(self):
#         selenium = self.selenium
#         selenium.get('http://127.0.0.1:8000/story-9/')
#         selenium.get('http://127.0.0.1:8000/story-9/books/')
#         time.sleep(5);
#         star = selenium.find_element_by_id('star')
#         star.click()
#         classes = star.get_attribute("class").split(" ")
#
#         time.sleep(10)
#         self.assertEqual('checked', classes[-1])
#
#     def test_button_add_fav(self):
#         selenium = self.selenium
#         selenium.get('http://127.0.0.1:8000/story-9/books/')
#         time.sleep(5);
#         star = selenium.find_element_by_id('star')
#         star.click()
#         text_fav = selenium.find_element_by_tag_name('h2')
#
#         time.sleep(10)
#         self.assertEqual(text_fav.text, "1 favorited")
