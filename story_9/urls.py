from django.urls import path, include
from .views import *

urlpatterns = [
    path('', home, name='home'),
    path('books/', books, name='books'),
    path('books/getulululululul/<book_name>', readJSON, name='json'),
    path('login/', login, name='login'),
    path('logout/', logout, name='logout'),
    path('auth/', include('social_django.urls', namespace='social')),
]
