from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponse
from django.http import HttpResponseRedirect
import requests
import json
from social_django.models import *
from django.forms.models import model_to_dict
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse

url = 'https://www.googleapis.com/books/v1/volumes?q='

def home(request):
	def to_serializer(val):
		return str(val)

	if 'user' not in request.session and str(request.user) != 'AnonymousUser':
		data = json.dumps(model_to_dict(request.user), default=to_serializer)
		request.session['user'] = {}
		request.session['user'][request.user.get_username()] = json.loads(data)
		request.session['books'] = []

	content = {'title' : 'Welcome!'}
	return render(request, 'home.html', content)

@csrf_exempt
def books(request):
	if request.method == "POST":
		data = request.POST
		title = data['title']
		flag = int(data['flag'])

		if flag == 0:
			lis = request.session['books']
			lis.append(title)
			request.session['books'] = lis
		else:
			lis = request.session['books']
			lis.remove(title)
			request.session['books'] = lis

		counter = len(request.session['books'])
		return JsonResponse({'books' : request.session['books'], 'counter' : counter})

	content = {'title' : 'Recommended Books', 'counter' : 0}
	return render(request, 'books.html', content)

def readJSON(request, book_name):
	response = requests.get(url + book_name)
	cook = request.session['books']
	counter = len(cook)
	data = response.json()
	books = {
		'book' : [],
		'favorited_books' : cook,
		'counter' : counter,
	}

	for item in data['items']:
		book_temp = {}
		book_temp['title'] = item['volumeInfo']['title']
		book_temp['authors'] = item['volumeInfo'].get('authors', ["Anonim"])[0]
		book_temp['added'] = False
		books['book'].append(book_temp)

	json_data = json.dumps(books)

	return HttpResponse(json_data, content_type="application/json")

def login(request):
	content = {'title' : 'Login'}
	return render(request, 'login.html', content)

def logout(request):
	request.session.flush()
	content = {'title' : 'Welcome!'}
	return render(request, 'home.html', content)
