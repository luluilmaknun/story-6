from django.db import models
import datetime

class Status(models.Model):
    dates = models.DateTimeField(auto_now_add=True)
    status = models.TextField(max_length=300)
