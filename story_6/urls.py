from django.urls import path
from .views import *

urlpatterns = [
    path('', home, name='home'),
    path('add_status/', add_status, name='add_status'),
	path('about/', about, name='about')
]
