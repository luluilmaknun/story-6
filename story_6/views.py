from django.shortcuts import render
from django.shortcuts import redirect
from .forms import StatusForm
from .models import Status
from datetime import datetime, date

curr_year = int(datetime.now().strftime("%Y"))
name = 'Lulu Ilmaknun Qurotaini'
moto = 'Be Free, Be New, Be Different'

def home(request):
	data = Status.objects.all()
	form = StatusForm()

	content = {'title' : 'My Status',
			'form' : form,
			'text' : 'Hello apa kabar?',
			'data' : data}
	return render(request, 'landing.html', content)

def about(request):
	content = {'title' : 'Profile',
				'text' : 'My Profile',
				'name' : name,
				'moto' : moto,
				'age' : calculate_age(1999)}
	return render(request, 'about_me.html', content)
	
def calculate_age(birth_year):
	return curr_year - birth_year if birth_year <= curr_year else 0
	
def add_status(request):
	form = StatusForm(request.POST)
	if form.is_valid():
		form.save()
	return redirect('home')