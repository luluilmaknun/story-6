from django.test import TestCase, Client
from django.urls import resolve
from django.utils import timezone
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import *
from .models import Status
from .forms import StatusForm
from datetime import date
import time

class Story6_Web_Test(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/story-6/')
        self.assertEqual(response.status_code, 200)

    def test_using_landing_template(self):
        response = Client().get('/story-6/')
        self.assertTemplateUsed(response, 'landing.html')

    def test_using_home_func(self):
        found = resolve('/story-6/')
        self.assertEqual(found.func, home)

    def test_helo(self):
        request = HttpRequest()
        response = home(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Hello apa kabar?', html_response)

class Story6_Web_About_Test(TestCase):
    def test_url_about_me_exist(self):
        response = Client().get('/story-6/about/')
        self.assertEqual(response.status_code, 200)

    def test_using_about_template(self):
        response = Client().get('/story-6/about/')
        self.assertTemplateUsed(response, 'about_me.html')

    def test_using_about_func(self):
        found = resolve('/story-6/about/')
        self.assertEqual(found.func, about)

    def test_calculate_age(self):
        self.assertEqual(0, calculate_age(date.today().year))
        self.assertEqual(18, calculate_age(2000))
        self.assertEqual(28, calculate_age(1990))

    def test_contains_content(self):
        request = HttpRequest()
        response = about(request)
        html_response = response.content.decode('utf8')
        self.assertIn(name, html_response)
        self.assertRegex(html_response, r'[0-9]\d+ years old')
        self.assertIn(moto, html_response)

class Story6_Model_Test(TestCase):
    def test_model_create_new_status(self):
        new_status = Status.objects.create(dates=timezone.now(), status='Aku lagi galau :(')

        counting_status_object = Status.objects.all().count()
        self.assertEqual(counting_status_object, 1)

    def test_save_POST_request(self):
        response = self.client.post('/story-6/add_status/', data={'dates' : '2018-10-10T14:46', 'status' : 'Mewe dulu gaaaannn'})
        counting_status_object = Status.objects.all().count()
        self.assertEqual(counting_status_object, 1)

        self.assertEqual(response.status_code, 302)

        new_response = self.client.get('/story-6/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Mewe dulu gaaaannn', html_response)

class Story6_Form_Test(TestCase):
    def test_forms_valid(self):
        form_data = {'status': 'something ahahhahahahahhahahhaha'}
        form = StatusForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_forms_not_valid(self):
        form_data = {'status': 'Membuat website baru yang membuat saya sedikit zbl. Karena kita diminta membuat herokuapp baru dan berarti repo baru. Saya berharap lab dan Story PPW bisa berjalan dalam satu repository saja hingga akhir, kalaupun dibutuhkan maksimal dua repository dan herokuapp. Tapi nyatanya sudah lebih dari 3 repo dan herokuapp baru yang dibutuhkan hingga sekarang. Tapi terlepas dari hal itu, membuat website dengan TDD membuka pandangan baru tentang website development. Pada awalnya pengerjaan TDD terlihat sangat meribetkan karena tentunya kita cenderung lebih suka mengetest website secara langsung di localhost. Tapi hal ini sebenarnya tidak efisien dan tidak bisa mengcover segala kesalahan yang mungkin akan terjadi di belakang website. Dalam membuat website dengan TTD tentu langkah pertama yang dilakukan adalah membuat project baru dan app baru, lalu melakukan configurasi setting seperti di story dan lab sebelumnya. Dan sebelum melakukan yang lainnya, kita bisa mulai membuat Unit Test di app. Dalam struktur Django, dalam setiap app akan tersedia test.py untuk melakukan TDD. Kita bisa mulai mengedit test dengan men-deklarasikan class untuk Unit Test. Dalam setiap Unit test kita bisa membuat sebuah fungsi untuk testing kerja website mulai dari response client hingga tulisan bisa kita test. Dalam satu file test kita bisa membuat banyak Unit Test dan fungsi test. Membuat Unit Test adalah salah satu bentuk dari TDD. Setelah semua test dibuat, saya membuat README.md berisi badge dan keterangan lebih lanjut. Badge dibuat untuk melihat coverage test terhadap program kita. Badge terdiri dari badge pipeline dan coverage. Untuk memproses coverage kita bisa memasukkan “TOTAL\s+\d+\s+\d+\s+(\d+)%” di general pipeline CI/CD gitlab. Lalu deploy ke gitlab.'}
        form = StatusForm(data=form_data)
        self.assertFalse(form.is_valid())

class Story6_Functional_Test(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story6_Functional_Test,self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story6_Functional_Test, self).tearDown()

    def test_input_status(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/story-6/')
        time.sleep(5);
        status = selenium.find_element_by_name('status')
        submit = selenium.find_element_by_id('submit')

        status_message = 'pepeeww gaan'
        status.send_keys(status_message)
        submit.click()
        time.sleep(10)

    def test_element_html_1(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/story-6/')
        time.sleep(5);
        title = selenium.find_element_by_tag_name('h1')
        self.assertEqual(title.text, 'My Status')

    def test_element_html_2(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/story-6/about/')
        time.sleep(5);
        p = selenium.find_element_by_tag_name('p')
        self.assertEqual(p.text, '"Be Free, Be New, Be Different"')

    def test_css_1(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/story-6/')
        time.sleep(5);
        css = selenium.find_element_by_class_name('left')
        self.assertEqual(selenium.find_element_by_id('form'), css)

    def test_css_2(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/story-6/about/')
        time.sleep(5);
        css = selenium.find_element_by_class_name('myfoto')
        self.assertEqual(selenium.find_element_by_id('profile_img'), css)

    def test_change_theme_header(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/story-6/')
        time.sleep(5);
        css = selenium.find_element_by_tag_name('header')
        background1 = css.value_of_css_property('background')

        button = selenium.find_element_by_id('theme')
        button.click()

        background2 = css.value_of_css_property('background')
        self.assertNotEqual(background1, background2)

    def test_change_theme_background(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/story-6/')
        time.sleep(5);
        button = selenium.find_element_by_id('theme')
        button.click()

        css = selenium.find_element_by_tag_name('body')
        background = css.value_of_css_property('background-color')
        self.assertEqual(background, 'rgba(20, 38, 52, 1)')

    def test_accordion(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/story-6/about/')
        time.sleep(5);
        accs = selenium.find_element_by_class_name('accordion')
        panel = selenium.find_element_by_class_name('panel')

        disp = panel.value_of_css_property('display')
        self.assertEqual(disp, 'none')

        accs.click()

        disp = panel.value_of_css_property('display')
        self.assertEqual(disp, 'block')
