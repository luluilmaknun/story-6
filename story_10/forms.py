from django import forms
from .models import Registrant
from django.forms import ModelForm

class RegistrantForm(ModelForm):
    class Meta:
        model = Registrant
        fields = ['email', 'nama', 'password']
        widgets = {
            'email' : forms.EmailInput(attrs={'class': 'form-control',
                                        'type' : 'email',
                                        'id' : 'email',
                                        'placeholder' : "your email pls.."}),
            'nama' : forms.TextInput(attrs={'class': 'form-control',
                                        'type' : 'text',
                                        'id' : 'nama',
                                        'placeholder' : "your name.."}),
            'password' : forms.PasswordInput(attrs={'class': 'form-control',
                                        'type' : 'password',
                                        'id' : 'password',
                                        'placeholder' : "password"})
        }
