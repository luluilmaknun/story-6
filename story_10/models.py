from django.db import models

class Registrant(models.Model):
    email = models.EmailField(unique= True, null=False, blank=False)
    nama = models.CharField(max_length=80, null=False, blank=False)
    password = models.CharField(max_length=50, null=False, blank=False)
