from django.test import TestCase, Client
from django.urls import resolve
from django.utils import timezone
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import *
from .models import Registrant
from .forms import RegistrantForm
import time

class Story10_Web_Test(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/story-10/')
        self.assertEqual(response.status_code, 200)

    def test_using_landing_template(self):
        response = Client().get('/story-10/')
        self.assertTemplateUsed(response, 'regist.html')

    def test_using_home_func(self):
        found = resolve('/story-10/')
        self.assertEqual(found.func, regist)

    def test_email_val(self):
        found = resolve('/story-10/email_val/ulu.ilmaknun.q@gmail.com_Lulu_password')
        self.assertEqual(found.func, email_val)

    def test_add_subscriber(self):
        found = resolve('/story-10/add_subscriber/')
        self.assertEqual(found.func, add_subscriber)

    def test_subscribe_json(self):
        found = resolve('/story-10/subscriber/')
        self.assertEqual(found.func, readJSON)

    def test_delete_subscriber(self):
        new_subscriber = Registrant.objects.create(email="amir.amirudin@gmail.com", nama='Amir Amirudin', password='amirpalinggantengsedunia')

        found = resolve('/story-10/delete_subscriber/amir.amirudin@gmail.com_amirpalinggantengsedunia')
        self.assertEqual(found.func, delete_subscriber)
        counting_status_object = Registrant.objects.all().count()
        self.assertEqual(1, counting_status_object)

class Story10_Model_Test(TestCase):
    def test_model_create_new(self):
        new_subscriber = Registrant.objects.create(email="amir.amirudin@gmail.com", nama='Amir Amirudin', password='amirpalinggantengsedunia')

        counting_status_object = Registrant.objects.all().count()
        self.assertEqual(counting_status_object, 1)

class Story10_Form_Test(TestCase):
    def test_forms_valid(self):
        form_data = {"email" : "amir.amirudin@gmail.com", "nama" : 'Amir Amirudin', "password" : 'amirpalinggantengsedunia'}
        form = RegistrantForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_forms_not_valid(self):
        form_data = {"email" : "gotta make it not valid", "nama" : 'Amir Amirudin', "password" : 'amirpalinggantengsedunia'}
        form = RegistrantForm(data=form_data)
        self.assertFalse(form.is_valid())
