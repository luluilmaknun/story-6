from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponse
from .forms import RegistrantForm
from .models import Registrant
from django.http import JsonResponse
from django import forms
from django.core import serializers
import json

# Create your views here
def regist(request):
	obj = {}
	form = RegistrantForm()

	content = {'title' : 'Subscribe',
			'form' : form,
            'text' : 'Be my subscriber!'}
	return render(request, 'regist.html', content)

def email_val(request, nama, email, password):
	data = { 'is_taken': Registrant.objects.filter(email__iexact=email).exists()}

	if '@' not in email:
		data['error_message'] = 'Masukkan format data yang benar'
		data['ok'] = 0
	elif data['is_taken']:
		data['error_message'] = 'Sudah terdaftar!<br>Email tersebut sudah pernah didaftarkan sebelumnya, silakan daftar dengan email lain'
		data['ok'] = 0
	else:
		data['error_message'] = 'Belum terdaftar'
		data['ok'] = 1

	if data['ok'] == 1:
		obj['email'] = email
		obj['nama'] = nama
		obj['password'] = password

	return JsonResponse(data)

def add_subscriber(request):
	data = {}
	Registrant.objects.create(email=obj['email'], nama=obj['nama'], password=obj['password'])
	data['message'] = 'Data berhasil disimpan'

	return JsonResponse(data)

def readJSON(request):
	data_string = serializers.serialize("json", Registrant.objects.all())
	data = json.loads(data_string)
	length = Registrant.objects.all().count()
	data_json = {'length' : length, 'data' : data}

	return JsonResponse(data_json)

def delete_subscriber(request, email, password):
	print("masuk")
	if Registrant.objects.filter(password__iexact=password).exists() & Registrant.objects.filter(email__iexact=email).exists():
		Registrant.objects.filter(email__iexact=email).delete()

	data_string = serializers.serialize("json", Registrant.objects.all())
	data = json.loads(data_string)
	length = Registrant.objects.all().count()
	data_json = {'length' : length, 'data' : data}

	return JsonResponse(data_json)
