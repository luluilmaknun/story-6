$(document).ready(function(){
  $('button[type="button"]').prop('disabled', true);

  $(document).keypress(function(e){
      if(e.which === 13){
        $('#subscribe').click();
      }
  });

  check = function () {
    email = $('#email').val();
    nama = $('#nama').val();
    password = $('#password').val();
    $.ajax({
      url: 'email_val/' + email + "_" + nama + "_" + password,
      dataType: 'json',
      success: function(response){
        $('#email_status').html(response.error_message);
        if(response.ok === 1){
          $('button[type="button"]').prop('disabled', false);
        }
      }
    })
  }

  $(document).on("change", "#email, #nama, #password", check);

  $(document).on('click', '#subscribe', function(){
    $.ajax({
      url: 'add_subscriber/',
      dataType: 'json',
      success: function(response){
        alert(response.message);
      }
    });
    $('#form').trigger("reset");
    $('#email_status').empty();
    $('button[type="button"]').prop('disabled', true);
  });

  $(document).on('click', 'button[id="delete_subscriber"]', function(){
    email = $(this).closest('td').prev().text();
    console.log(email);
    password = prompt("Masukkan password");
    $.ajax({
      url: 'delete_subscriber/' + email + '_' + password,
      dataType: 'json',
      success: function(response){
        $('#table_subscriber').empty();
        for(i = 0; i < response.length; i++){
            data = response.data[i].fields
            var tmp = "<tr><td>" + data.nama + "</td><td id='email'>" + data.email + "</td><td>" +
            "<button id='delete_subscriber' class='btn btn-icon btn-danger'><i class='fa fa-trash'></i></button></td></tr>";
            $('#table_subscriber > tbody').append(tmp);
        }
      }
    });
  });

  $.ajax({
    type: "GET",
    url: 'subscriber/',
    dataType: 'json',
    success: function(response){
      for(i = 0; i < response.length; i++){
          data = response.data[i].fields
          var tmp = "<tr><td>" + data.nama + "</td><td id='email'>" + data.email + "</td><td>" +
          "<button id='delete_subscriber' class='btn btn-icon btn-danger'><i class='fa fa-trash'></i></button></td></tr>";
          $('#table_subscriber > tbody').append(tmp);
      }
    }
  });
})
