from django.urls import path
from .views import *

urlpatterns = [
    path('', regist, name='regist'),
    path('email_val/<email>_<nama>_<password>', email_val, name='email_val'),
    path('add_subscriber/', add_subscriber, name='add_subscriber'),
    path('subscriber/', readJSON, name='show_subscriber'),
    path('delete_subscriber/<email>_<password>', delete_subscriber, name='delete_subscriber')
]
