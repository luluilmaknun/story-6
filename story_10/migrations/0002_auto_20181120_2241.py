# Generated by Django 2.1.1 on 2018-11-21 06:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story_10', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='registrant',
            name='email',
            field=models.EmailField(max_length=254, unique=True),
        ),
    ]
